let listId = new Array();
let listString = new Array();
let completedlist = new Array();
let count = 1;

function addItem(){
    let ul = document.getElementById("dynamic-list");
    let item = document.getElementById("item");
    let li = document.createElement("li");
    li.setAttribute('id',item.value + count);
    li.addEventListener("dblclick", myScript); 
    listId.push(item.value + count);
    listString.push(item.value);
    li.appendChild(document.createTextNode(item.value));
    ul.appendChild(li);
    count++; 
    document.getElementById("emptylist").disabled = false;
    document.getElementById("save").disabled = false;
    document.getElementById("add").disabled = true;
    item.value = null;
}

 function myScript(event) { 
    console.log('Ouch! Stop poking me!'); 
    let target = event.target;
    const text = target.id;
    if(target.classList != null && target.classList!= undefined &&
        target.classList.contains('completed')) {
            completedlist.pop(text);
            target.classList.remove('completed');
            if(listId.length == 0) {
                document.getElementById("clearcomp").disabled = true;
            } 
    } else { 
        completedlist.push(text);
        target.classList.add('completed'); 
        if(listId.length != 0) {
            document.getElementById("clearcomp").disabled = false;
        }
    }
} 


function removeCompletedList() {
    completedlist.forEach(str => {
        let element = document.getElementById(str);
        if(element != null && element!= undefined &&
             element.classList != null && element.classList!= undefined &&
             element.classList.contains('completed')) {
            element.parentNode.removeChild(element);
            listId.pop(str);
            listString.pop(element.innerText)
        }
    }); 
    completedlist = new Array();
    document.getElementById("clearcomp").disabled = true; 
    if(listId.length= 0) {
        document.getElementById("emptylist").disabled = true;
    } 
}

function emptylist() {
    listId.forEach(str => {
        let element = document.getElementById(str);
        element.parentNode.removeChild(element);
    }); 
    listId = new Array();
    listString = new Array();
    completedlist = new Array();
    document.getElementById("clearcomp").disabled = true;
    document.getElementById("emptylist").disabled = true;
    document.getElementById("save").disabled = true;
}

function saveList() {
    if(listId.length != 0) {
        let x = document.getElementById("second_div");
        x.style.display = "block";
    } 
    listId.forEach(str => {
        let ul = document.getElementById("dynamic-list2");
        let item = document.getElementById("item");
        let li = document.createElement("li");  
        li.appendChild(document.createTextNode(item.value));
        ul.appendChild(li);
    });
}

function onBlur() {
    let item = document.getElementById("item");
    if(item.value != null && item.value != undefined && item.value != '') {
        document.getElementById("add").disabled = false;
    } else {
        document.getElementById("add").disabled = true;
    }
}