let listId = [];
let listString = [];
let completedlist =[];
let count = 1;

function addItem(){
    let ul = document.getElementById("dynamic-list");
    let item = <HTMLInputElement>document.getElementById("item");
    let li = document.createElement("li");
    li.setAttribute('id',item.value + count);
    li.addEventListener("dblclick", myScript); 
    listId.push(item.value + count);
    listString.push(item.value);
    li.appendChild(document.createTextNode(item.value));
    ul.appendChild(li);
    count++; 
    let emptyBtn =  <HTMLInputElement> document.getElementById("emptylist")
    emptyBtn.disabled = false;
    let saveBtn =  <HTMLInputElement> document.getElementById("save")
    saveBtn.disabled = false;
    let addBtn =  <HTMLInputElement> document.getElementById("add")
    addBtn.disabled = true; 
    item.nodeValue = null;
}

 function myScript(event) { 
    console.log('Ouch! Stop poking me!'); 
    let target = event.target;
    const text = target.id;
    if(target.classList != null && target.classList!= undefined &&
        target.classList.contains('completed')) { 
            RemoveElementFromCompleteList(text);
            target.classList.remove('completed');
            if(listId.length == 0) {
                let clearBtn =  <HTMLInputElement> document.getElementById("clearcomp")
                clearBtn.disabled = true;  
            } 
    } else { 
        completedlist.push(text);
        target.classList.add('completed'); 
        if(listId.length != 0) {
            let clearBtn =  <HTMLInputElement> document.getElementById("clearcomp")
            clearBtn.disabled = false; 
        }
    }
}

function removeCompletedList() {
    completedlist.forEach(str => {
        let element =<HTMLInputElement> document.getElementById(str);
        if(element != null && element!= undefined &&
             element.classList != null && element.classList!= undefined &&
             element.classList.contains('completed')) {
            element.parentNode.removeChild(element);
            RemoveElementFromListId(str);
            RemoveElementFromListString(element.value)
        }
    }); 
    completedlist = new Array();
    let clearBtn =  <HTMLInputElement> document.getElementById("clearcomp")
    clearBtn.disabled = true; 
    if(listId.length= 0) { 
        let emptyBtn =  <HTMLInputElement> document.getElementById("emptylist")
        emptyBtn.disabled = true;
    } 
}

function emptylist() {
    listId.forEach(str => {
        let element = document.getElementById(str);
        element.parentNode.removeChild(element);
    }); 
    listId = new Array();
    listString = new Array();
    completedlist = new Array();
    let emptyBtn =  <HTMLInputElement> document.getElementById("emptylist")
    emptyBtn.disabled = true;
    let clearBtn =  <HTMLInputElement> document.getElementById("clearcomp")
    clearBtn.disabled = true; 
    let saveBtn =  <HTMLInputElement> document.getElementById("save")
    saveBtn.disabled = true;
}

function saveList() {
    if(listId.length != 0) {
        let x = document.getElementById("second_div");
        x.style.display = "block";
    } 
    listId.forEach(str => {
        let ul = document.getElementById("dynamic-list2");
        let item =<HTMLInputElement> document.getElementById("item");
        let li = document.createElement("li");  
        li.appendChild(document.createTextNode(item.value));
        ul.appendChild(li);
    });
}

function onBlur() {
    let item =<HTMLInputElement> document.getElementById("item");
    if(item.value != null && item.value != undefined && item.value != '') {
        let addBtn =  <HTMLInputElement> document.getElementById("add")
        addBtn.disabled = false; 
    } else {
        let addBtn =  <HTMLInputElement> document.getElementById("add")
        addBtn.disabled = true; 
    }
}


function RemoveElementFromListId(element: string) {
    this.listId.forEach((value,index)=>{
        if(value==element) this.arrayElements.splice(index,1);
    });
}

function RemoveElementFromListString(element: string) {
    this.listString.forEach((value,index)=>{
        if(value==element) this.arrayElements.splice(index,1);
    });
}

function RemoveElementFromCompleteList(element: string) {
    this.completedlist.forEach((value,index)=>{
        if(value==element) this.arrayElements.splice(index,1);
    });
}